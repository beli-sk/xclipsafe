XClipSafe
=========

Regular X clipboard clearing, for cases when you forget some sensitive
information in it and paste it in the wrong window. Works on all buffers,
PRIMARY, SECONDARY and CLIPBOARD. By default checks every 60 seconds and
if contents is the same as on the last check, it is cleared. So any data
you put in clipboard are cleared after 1 to 2 minutes.

The program should be started as a background job on user's XSession.


Locations
---------

The `XClipSafe project page`_ is hosted on Bitbucket.

If you've never contributed to a project on Bitbucket, there is
a `quick start guide`_.

If you find something wrong or know of a missing feature, please
`create an issue`_ on the project page.

.. _XClipSafe project page:      https://bitbucket.org/beli-sk/xclipsafe
.. _quick start guide: https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo%2C+Compare+Code%2C+and+Create+a+Pull+Request
.. _create an issue:   https://bitbucket.org/beli-sk/xclipsafe/issues


License
-------

Copyright 2016 Michal Belica <devel@beli.sk>

::

    XClipSafe is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    XClipSafe is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with XClipSafe.  If not, see < http://www.gnu.org/licenses/ >.

A copy of the license can be found in the ``LICENSE`` file in the
distribution.

