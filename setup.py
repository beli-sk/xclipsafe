#!/usr/bin/env python3
#
# XClipSafe - regular X clipboard clearing
# Copyright (C) 2016  Michal Belica <devel@beli.sk>
#
# This file is part of XClipSafe.
#
# XClipSafe is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# XClipSafe is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with XClipSafe.  If not, see <http://www.gnu.org/licenses/>.
#
from setuptools import setup, find_packages
from codecs import open # To use a consistent encoding
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

# get version number
defs = {}
with open(path.join(here, 'xclipsafe/defs.py')) as f:
    exec(f.read(), defs)

setup(
    name='xclipsafe',
    version=defs['__version__'],
    description=defs['app_description'],
    long_description=long_description,
    #url='',
    author="Michal Belica",
    author_email="devel@beli.sk",
    license="GPL-3",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: No Input/Output (Daemon)',
        'Environment :: X11 Applications',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        ],
    zip_safe=True,
    packages=['xclipsafe'],
    entry_points={
        'console_scripts': [
            'xclipsafe = xclipsafe:main',
            ],
        },
    )

