#
# XClipSafe - regular X clipboard clearing
# Copyright (C) 2016  Michal Belica <devel@beli.sk>
#
# This file is part of XClipSafe.
#
# XClipSafe is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# XClipSafe is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with XClipSafe.  If not, see <http://www.gnu.org/licenses/>.
#
import time
import logging
import subprocess

from .xenum import XEnum


# watch interval, if any clipboard does not change between checks, it is cleared
INTERVAL = 5 # seconds

# subprocess timeout
TIMEOUT = 3 # seconds

xclip = XEnum((
        ('-p', 'PRIMARY'),
        ('-s', 'SECONDARY'),
        ('-b', 'CLIPBOARD'),
        ))

logger = logging.getLogger(__name__)


class XClipSafe(object):

    def __init__(self):
        self.logger = logger
        self.clip_contents = dict()

    def start_watching(self):
        while True:
            for clip in xclip.keys():
                cc = self._get_clip(clip)
                if clip in self.clip_contents and self.clip_contents[clip] == cc:
                    self._clear_clip(clip)
                    self.clip_contents[clip] = None
                    self.logger.info('%s buffer cleared', xclip[clip])
                elif cc != b'':
                    self.clip_contents[clip] = cc
            time.sleep(INTERVAL)

    def _get_clip(self, clip):
        return subprocess.check_output(('xsel', clip, '-o'), timeout=TIMEOUT)

    def _clear_clip(self, clip):
        subprocess.check_call(('xsel', clip, '-c'), timeout=TIMEOUT)


def main():
    logging.basicConfig(level=logging.INFO)
    XClipSafe().start_watching()

